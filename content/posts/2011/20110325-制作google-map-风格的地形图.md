---
title: 制作google map 风格的地形图
date: 2011-03-25T06:40:51+00:00
categories:
  - 户外
tags:
  - 导航
  - 地图 
---
先看个最终效果，还过得去吧。

![my](/images/2011/googleterria-my.jpg)

要问为什么喜欢Google，其中一个原因就是简约，精炼的风格，包括Google map的地形图。Google地形图看起来就这样子。

![google](/images/2011/googleterria-google.jpg)

要想看到google地形图，可以通过http://map.google.com中选择Terrian。另外在google earth中也可以通过叠加来实现，这个地址可以很容易的附加上地形图层[http://www.mgmaps.com/kml](http://www.mgmaps.com/kml/)。

如果想自己制作也不是很难，虽然和google的还有些区别，但是也算接近吧。

1. 打开GM
2. Tool->Configure->ShaderOption
3. Slope Shader->Minimum Shape –>点Slope Value选择Degree，输入0.
4. Slope Value右边的color，在RGB中输入237，242，225
5. Maxium Shape->Slope Value, Dgree 选45
6. SlopeValue右边Color，RGB值70，79，53
7. Color between Min and Max 选择Smooth
8. 在工具栏中选择Slope Shader，并开启阴影。

当然，需要将SRTM的数据加载。